import "antd/dist/antd.css"
import {Steps} from 'antd';
import React, {useState} from "react";
import FormStep1 from "./FormStep1";
import FormStep2 from "./FormStep2";
import FormStep3 from "./FormStep3";
import FormStep4 from "./FormStep4";
import './Card.css';
const { Step } = Steps;

function Card(){
    const [stepCount, setCount] = useState(0);
    const [userEmail, setEmail] = useState();

    function submit(values: any){
        setCount(stepCount+1)
        if (values.email){
            setEmail(values.email)
        }
    }

    function FormType(props: any){
        switch (stepCount){
            case 0:
                return <FormStep1 submitData = {submit}/>
            case 1:
                return <FormStep2 submitData = {submit}/>
            case 2:
                return <FormStep3 mail = {userEmail} submitData = {submit}/>
            case 3:
                return <FormStep4/>
        }
        return <></>
    }

    return(
        <>
            <FormType step = {stepCount}/>
            <Steps current={stepCount}>
                <Step title="1" />
                <Step title="2" />
                <Step title="3" />
                <Step title="4" />
            </Steps>
            </>)
}

export default Card