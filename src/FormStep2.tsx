import React, {useEffect, useState} from "react";
import "antd/dist/antd.css"
import {Input, Form, Button, Typography} from "antd";
import { Badge } from 'antd';
import "./FormStep2.css";

const {Title} = Typography;

function FormStep2(props: any){
    const [form] = Form.useForm();
    const [timer, setCurrent] = useState(60);
    const [disabled, setDisabled] = useState(true)
    let delay: any;

    function startTimer(){
        let current = 60;
        delay = setInterval(()=>{
            if (current>0){
                setCurrent(current-1)
                current-=1;
            }
            else {
                clearInterval(delay);
                setDisabled(false)
            }
        }, 1000)
    }

    async function onFinish(values: any){
        return new Promise<any>(resolve => {
            setTimeout(()=>{
                const value: number = Math.random();
                if (value>0.5){
                    props.submitData(values);
                    resolve("Success")
                }
                else {
                    alert("Fail")
                    resolve("Fail")
                }
            }, 2000)
        })
    }

    useEffect(()=>{
        startTimer();
    }, []);
    return <Form className="form" labelCol={{span: 8}} wrapperCol={{span: 8}} name='restore' form={form} onFinish={onFinish}>
    <Title level = {4}>Письмо отправлено проверьте почту</Title>
        <Form.Item label="Код" name="code">
            <Input/>
        </Form.Item>
        <Form.Item wrapperCol = {{ offset: 4, span: 16 }}>
            <Button type="primary" htmlType="submit">Сбросить пароль</Button>
            <Badge count={timer}>
                <Button disabled={disabled} className='btn-timer' htmlType="button">Отправить письмо повторно</Button>
            </Badge>
            <Button htmlType="button">Отмена</Button>
        </Form.Item>
    </Form>
}

export default FormStep2;