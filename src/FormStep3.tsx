import React, {useState} from "react";
import "antd/dist/antd.css"
import {Input, Form, Button} from "antd";
import "./FormStep3.css";

function FormStep3(props: any){
    const [form] = Form.useForm();

    async function onFinish(values: any){
        return new Promise<any>(resolve => {
            setTimeout(()=>{
                const value: number = Math.random();
                if (value>0.5){
                    props.submitData(values);
                    resolve("Success")
                }
                else {
                    alert("Fail")
                    resolve("Fail")
                }
            }, 2000)
        })
    }

    return <Form className="form" labelCol={{span: 8}} wrapperCol={{span: 8}} name='restore' form={form} onFinish={onFinish}>
    <Form.Item label="Email" name="mail" rules={[{required: true, message: 'Введите вашу почту'}]}>
            <Input defaultValue={props.mail}/>
        </Form.Item>
        <Form.Item label="Новый пароль" name="passwordNew" rules={[{required: true, message: 'Введите новый пароль'}]}>
            <Input/>
        </Form.Item>
        <Form.Item label="Потвердите новый пароль" name="passwordRepeat" rules={[{required: true, message: 'Введите новый пароль еще раз'}]}>
            <Input/>
        </Form.Item>
        <Form.Item wrapperCol = {{ offset: 4, span: 16 }}>
            <Button type="primary" htmlType="submit">Сбросить пароль</Button>
            <Button htmlType="button">Отмена</Button>
        </Form.Item>
    </Form>
}

export default FormStep3;