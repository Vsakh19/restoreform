import React from "react";
import {Input, Form, Button} from "antd";
import "./FormStep1.css";

function FormStep1(props: any){
    const [form] = Form.useForm();

    async function onFinish(values: any){
        return new Promise<any>(resolve => {
            setTimeout(()=>{
                const value: number = Math.random();
                if (value>0.5){
                    props.submitData(values);
                    resolve("Success")
                }
                else {
                    alert("Fail")
                    resolve("Fail")
                }
            }, 2000)
        })
    }

    return <Form className="form" labelCol={{span: 8}} wrapperCol={{span: 8}} name='restore' form={form} onFinish={onFinish}>
        <Form.Item label="Email" name="email" rules={[{required: true, message: 'Введите вашу почту для восстановления' +
                    ' пароля'}]}>
                <Input/>
            </Form.Item>
            <Form.Item wrapperCol = {{ offset: 8, span: 8 }}>
                <Button type="primary" htmlType="submit">Отправить письмо</Button>
                <Button htmlType="button">Отмена</Button>
            </Form.Item>
    </Form>
}

export default FormStep1;