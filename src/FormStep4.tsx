import React from "react";
import "antd/dist/antd.css"
import {Form, Button, Typography} from "antd";
import "./FormStep4.css";

const {Title} = Typography;

function FormStep4(){
    const [form] = Form.useForm();
    return <Form className="form" labelCol={{span: 8}} wrapperCol={{span: 8}} name='restore' form={form}>
    <Title level = {4}>Пароль успешно сброшен</Title>
        <Form.Item wrapperCol = {{ offset: 4, span: 16 }}>
            <Button type="primary" htmlType="submit">Войти</Button>
        </Form.Item>
    </Form>
}

export default FormStep4;